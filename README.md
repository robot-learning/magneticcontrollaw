MagneticControlLaw


requires https://bitbucket.org/robot-learning/ll4ma-opt-sandbox/src/main/

follow install instructions for ll4ma-opt. Note for windows you will have to do commands manually

```conda env create --file ll4ma_opt_env.yml```
```conda activate ll4ma-opt```
```pip install -e .```

then need to activate ll4ma-opt conda environment
additionally ```conda install spyder ``` is suggested for IDE
to run python main.py
